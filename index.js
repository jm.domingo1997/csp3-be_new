
//Directive Sections
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//express function is the server stored in a constant
const app = express();

//Middlewares
app.use (express.json());
app.use (express.urlencoded ({extended:true}));
app.use (cors());

//Connecting routes modules to index.js entry point
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

//middleware entry point url
app.use('/users', userRoutes);
app.use('/products', productRoutes);




//connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.eqilw.mongodb.net/capstone2?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

//MongoDB Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));



//app server listenting to port 3000
const PORT = 3000;
app.listen(process.env.PORT || 3000, () => console.log (`Server is running at ${PORT}`));