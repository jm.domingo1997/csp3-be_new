
//directive 
const express = require('express');

//Roouter() handles the request
const router = express.Router();

//import authentification verify
const auth = require('./../auth.js');

//import productController
const productController = require('./../controllers/productController')

//Get All Products
router.get("/all", (req, res) => {
	productController.getAll().then(result => res.send(result));
})
//Get All Active Products
router.get("/active", (req, res) => {
	productController.getAllActive().then(result => res.send(result));
})
//Add Product (Admin Only)
router.post("/",auth.verify, (req, res) => {
	const checkAdmin = auth.decode(req.headers.authorization).isAdmin;
	//console.log(isAdmin)
	if(checkAdmin == true){
		productController.addProduct(req.body).then(result => res.send(result));
	}else{
		return res.send(`Not an Admin thus Request is rejected`);
	}
})
// Get Specific Product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(result => res.send(result));
})
//Update Product
router.put("/:productId", auth.verify, (req,res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);
	//console.log(isAdmin);
	if(isAdmin == true){
		productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}else{
		return res.send('Not Authorized');
	}
})
//Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);
	//console.log(isAdmin);
	if(isAdmin == true){
		
		//console.log(`true`);
		productController.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}else{
		//console.log(`false`);
		return res.send('Not Authorized');
	}
})
//Unarchive or Activate Product
router.put("/:productId/activate", auth.verify, (req,res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);
	//console.log(isAdmin);
	if(isAdmin == true){
		
		//console.log(`true`);
		productController.activateProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}else{
		//console.log(`false`);
		return res.send('Not Authorized');
	}
})
//productRoutes export
module.exports = router;