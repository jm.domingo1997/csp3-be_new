const Product = require('./../models/Product');
const bcrypt = require('bcrypt');
const auth = require(`./../auth`);

//Get All Product
module.exports.getAll = () => {
	return Product.find({}).then((result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};
//Get All Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};
//Add Product (Admin Only)
module.exports.addProduct = (reqBody) => {
	return Product.findOne({name:reqBody.name}).then( (result) =>{
		if(result){
			return `Duplicate Product was found`;
		}else{
		
		const {name,description,price} = reqBody;

		const addedProduct = new Product({
			name: name,
			description: description,
			price: price
		});
			
			return addedProduct.save().then((result,error) => {
				if(result){
					return result;
				}else{
					return false;
				}
			})
		}
	})
};
//Get Specific Product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};
//Update Product
module.exports.updateProduct = (productId, reqBody) => {
	const {name, description, price} = reqBody;
	const update = {name:name, description:description, price:price}
	
	return Product.findByIdAndUpdate({_id:productId}, update, {returnDocument:'after'}).then( (result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};
//Archive Product
module.exports.archiveProduct = (productId) => {
	//console.log(productId)
	//const filter = {_id : productId}
	//console.log(filter);
	const update = {isActive:false};
	//console.log(update)
	return Product.findByIdAndUpdate({_id:productId}, update, {returnDocument:'after'}).then( (result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};
//Unarchive or Activate Product
module.exports.activateProduct = (productId) => {
	//console.log(productId)
	//const filter = {_id : productId}
	//console.log(filter);
	const update = {isActive:true};
	//console.log(update)
	return Product.findByIdAndUpdate({_id:productId}, update, {returnDocument:'after'}).then( (result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};