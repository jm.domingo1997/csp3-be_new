const User = require('./../models/User');
const bcrypt = require('bcrypt');
const auth = require(`./../auth`);
const Product = require('./../models/Product');


//register method
module.exports.register = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) =>{
			if(result){
				return "The email was already taken!"
			}else{
				//reqBody deconstructuring
				const { email, password} = reqBody;

				const newUser = new User({
					email: email,
					password: bcrypt.hashSync(password,10)
				});

				return newUser.save().then( (result, error) => {
					if(result){
						return result;
					}else{
						return error;
					}
				})



			}
		})
};

//login method
module.exports.loginUser = (reqBody) => {
	const {email,password} = reqBody;

	return User.findOne({email:email}).then( (result) => {
		if(result == null){
			return "Please input an email and password";
		}

		else{
			let checkPwCorrect = bcrypt.compareSync(password, result.password)

			if(checkPwCorrect == true){
				return {access: auth.createAccessToken(result)}
			}else{
				return "Invalid Password";
			}
		}
	})
};

//Get All Users
module.exports.getAll = () => {
	return User.find({}).then((result,error) => {
		if(result){
			return result;
		}else{
			return error;
		}
	})
};

//Get orders
module.exports.getOrders = (id) => {
	return User.findById(id).then((result, error) => {
		if(result){
			return result.orders;
		}else{
			error;
		}
	})
};

// //checkOut
// module.exports.checkOut = async (id,reqBody) => {
// 	const productId = reqBody.products.productId;
// 	const userId = id;

// 	const updateUser = User.findById(userId).then( (result,error) => {
// 		//console.log(result);
// 		if(result){
// 			result.orders.push(reqBody);
// 			return result.save().then( result => {return true;}) 
// 		}else{
// 			return false;
// 		}

// 	// const orderId = await User.findById(userId).then( (result,error) => {
// 	// 	//console.log(result);
// 	// 	if(result){
// 	// 		console.log
// 	// 	}else{
// 	// 		return false;
// 	// 	}

// 	});

// 	// const userUpdate = await User.findById(userId).then( (result,error) => {
// 	// 	//console.log(result);
// 	// 	if(result){
// 	// 		result.orders.push(reqBody);
// 	// 		return result.save().then( result => {return true;})
// 	// 	}else{
// 	// 		return false;
// 	// 	}
// 	// });

// 	// const productUpdate = await Product.findById(productId).then( (result,error) =>{
// 	// 	if(result){}
// 	// })
// };

//getAllOrders
module.exports.getAllOrders = () => {
	const notIncluded = {
		_id: 0,
		email: 0,
		password: 0,
		isAdmin:0,
		__v: 0
	}
return User.find({}, notIncluded).then(result => {
		const unfiltered = result;
		const filtered = [];
		
		for(let i = 0; i < unfiltered.length; i++){
			if(unfiltered[i].orders.length !== 0 ){
				filtered.push(unfiltered[i].orders);
				}
		}
		//console.log(show)
		return filtered;
	});






		// const nullFilter = (item) => {
		// 	if( typeof item === 'object' && !Array.isArray(item) && Object.keys(item).length === 0 ){
		// 		return false;
		// 	}else{
		// 		return true;
		// 	};
		// }

		// console.log(result.filter(nullFilter));
		//  //console.log(typeof []);

		
	// const array = result.enrollments.filter((userId) =>{
	// 		return userId
	// 	});
		
	// 	if(array !="null"){
	// 		console.log("Duplicate was found");
	// 	}
	
}

//dummy checkout
module.exports.checkOut = (userId, cart) => {

return User.findById(userId).then(user => {
		if(user === null){
			return false;
		}else{
			//push the contents of the cart to user
			user.orders.push({
				products: cart.products,
				totalAmount: cart.totalAmount
			})
		
		//console.log(user.orders);
		return user.save().then((updatedUser, error) => {
			if(error){
				return false
			}else{
				//const index = updatedUser.orders.length-1
				const currentOrder = updatedUser.orders[updatedUser.orders.length-1]
				currentOrder.products.forEach( product => {
					//product.productId
					Product.findById(product.productId).then( foundProduct => {
						foundProduct.orders.push({orderId:currentOrder._id});
						foundProduct.save()
					})
				})

				return true;

				//current order can have multiple products
				//we need to get those produccts to access the product id
				//currentOrdere
				// _id:123
				/*products: [
                   {
						productName: "iPad",
						productId: 1 -using the productId, search the product collection
						findById(1)
						iPad.orders
						OrderId: _id
                   },
                   {
						productName: "MacBook Pro",
						productId: 2 -
                   }  
				]

				*/

			}
		})
	}
})
};
//set as user id
module.exports.setAsAdmin = (userId) => {

	return User.findById(userId).then((result,error) => {
		if(result.isAdmin == true){
			return `Already an Admin`;
		}else{
			return User.findByIdAndUpdate({_id:userId},{isAdmin:true},{returnDocument:'after'}).then((result,error) => {
				if(result){
					return result;
				}else{
					return error;
				}
			})
		}
	})
}

// const colors = ['red','green','blue'];
// colors.push()
// updatedUser.orders[updatedUser.orders.length-1]